import logo from './logo.svg';
import './App.css';
import Header from './Header';
import Page1 from './page1';
import Page2 from './page2';
import Page3 from './page3';
import Page4 from './page4';
import Footer from './footer';
function App() {
  return (
    <div className="App">
      <Header/>
      <Page1/>
      {/* <Page2/>
      <Page3/>
      <Page4/>
      <Footer/>
       */}
    </div>
  );
}

export default App;
